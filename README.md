# README #

This README should describe how to build and deploy my MIWTest application.

### What is this repository for? ###

* Simple Rest application developed for Mobile Integration Workgroup to see a highlight of my development skills.
* Version : 0.0.1

### How do I get set up? ###

* This application was developed with Java 1.8, Spring & Spring MVC 4.11. I ran it on Tomcat8 locally
* Checkout the repository and import into an Eclipse Maven Project.
* All dependencies should be contained in the WAR file that is built

### How to run tests ###
  - use the mvn test command

### Deployment instructions ###
 - build war file with mvn package command
- deploy war file into application server, I used Tomcat8.

### Use ###
 - To use the /items Rest Method, a browser pointed to http://localhost:8080/MIWTest/items for example. 

 - To use the /buyItems/{item name} method, a Rest client should be used, with a PUT method type. Basic authentication is enabled, 

user : bill,
password : abc123 

should allow access to the method. 


### Who do I talk to? ###

* Todd Wuolle todd.wuolle@gmail.com