package com.tw.gildedrose.controller;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.tw.gildedrose.dao.ItemDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration

@ContextConfiguration
public class ItemRestControllerTest {

	private MockMvc mockMvc;

	@Configuration
	static class ItemsRestControllerTestConfiguration {
		@Bean
		public ItemDAO itemDAO() {
			return Mockito.mock(ItemDAO.class);
		}

		@Bean
		public HttpSession httpSession() {
			return Mockito.mock(HttpSession.class);
		}

		@Bean
		public FilterChainProxy springSecurityFilterChain() {
			return Mockito.mock(FilterChainProxy.class);
		}

	}

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	protected MockServletContext mockServletContext;

	@Autowired
	FilterChainProxy springSecurityFilterChain;

	@Before
	public void setup() {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);

		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();

	}

	@Test
	public void testGetItems() {

		String expectedJSONValue = "{'name':'paper','description': 'ream of paper','price':2},{'name':'towel','description': 'a cloth towel','price':5},{'name':'bottle','description':'1.5L bottle','price':2}";

		try {
			this.mockMvc.perform(MockMvcRequestBuilders.get("/items")).andExpect(MockMvcResultMatchers.status().isOk())
					.andExpect(MockMvcResultMatchers.jsonPath(expectedJSONValue).isArray());

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Test
	@WithMockUser(username = "bill", password = "abc123")
	public void testBuyItem() {

		try {

			this.mockMvc.perform(MockMvcRequestBuilders.put("/buyItem/towel"))
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	@WithMockUser(username = "bill", password = "abc123")
	public void testIncorrectBuyItem() {

		try {

			this.mockMvc.perform(MockMvcRequestBuilders.put("/buyItem/stapler"))
					.andExpect(MockMvcResultMatchers.status().is(HttpStatus.EXPECTATION_FAILED.value()));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
