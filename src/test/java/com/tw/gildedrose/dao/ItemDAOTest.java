package com.tw.gildedrose.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.tw.gildedrose.config.ItemDAOTestConfig;
import com.tw.gildedrose.model.Item;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ItemDAOTestConfig.class, loader = AnnotationConfigContextLoader.class)
public class ItemDAOTest {

	@Autowired
	private ItemDAO itemDAO;

	@Test
	public void testGetItemsFindList() {

		List<Item> foundItems = itemDAO.getItems();

		assertTrue("Items were not found", !foundItems.isEmpty());
	}

	@Test
	public void testUpdateInventorySuccess() {

		String itemName = "bottle";

		assertTrue(itemDAO.updateInventory(itemName));

		itemName = "towel";

		assertTrue(itemDAO.updateInventory(itemName));

	}
	
	@Test
	public void testUpdateInventoryAfterRemoval() {
		
		String itemName = "paper";

		itemDAO.updateInventory(itemName);
		
		assertFalse("item still present", itemDAO.updateInventory(itemName));
		
		
	}
	
	@Test
	public void testUpdateInventoryItemNotFound() {
		
		String itemName = "tissue";

		assertFalse(itemDAO.updateInventory(itemName));
		
	}

}
