package com.tw.gildedrose.controller;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tw.gildedrose.dao.ItemDAO;
import com.tw.gildedrose.model.Item;
import com.tw.gildedrose.model.json.ItemReturnVO;

/**
 * A Spring Rest Controller to provide 2 methods, /items and /buyItem/. 
 * 
 * @author Todd
 *
 */
@RestController

public class ItemsRestController {

	@Autowired
	private ItemDAO itemDAO;

	@GetMapping("/items")
	public List<Item> getItems() {
		return itemDAO.getItems();
	}

	@PutMapping("/buyItem/{itemId}")
	@ResponseBody
	public ResponseEntity<ItemReturnVO> buyItem(@PathVariable String itemId) {

		if (itemDAO.updateInventory(itemId)) {
			ItemReturnVO returnVO = new ItemReturnVO(HttpStatus.OK.toString(), "Item bought");

			return new ResponseEntity<ItemReturnVO>(returnVO, HttpStatus.OK);
		} else {
			ItemReturnVO returnVO = new ItemReturnVO(HttpStatus.EXPECTATION_FAILED.toString(),
					"Item not found in inventory");
			return new ResponseEntity<ItemReturnVO>(returnVO, HttpStatus.EXPECTATION_FAILED);
		}
	}

}
