package com.tw.gildedrose.model;

/**
 * A Simple POJO class to represent the item for sale
 * 
 * @author Todd
 *
 */
public class Item {

	private String Name;
	private String Description;
	private int Price;
	
	
	public Item(String name, String description, int price) {
		super();
		Name = name;
		Description = description;
		Price = price;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public int getPrice() {
		return Price;
	}
	public void setPrice(int price) {
		Price = price;
	}
	
	
}
