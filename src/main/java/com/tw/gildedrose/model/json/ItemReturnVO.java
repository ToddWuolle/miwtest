package com.tw.gildedrose.model.json;

import java.io.Serializable;

/**
 * This is a simple POJO to return the status from the Rest request.
 * 
 * @author Todd
 *
 */
public class ItemReturnVO implements Serializable {

	
	private static final long serialVersionUID = 8983895605960510467L;
	
	
	private String status;
	private String message;
	
	
	public ItemReturnVO(String status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
