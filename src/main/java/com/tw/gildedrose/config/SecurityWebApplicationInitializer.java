package com.tw.gildedrose.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * An empty class that needs to be present for the Security Web application initializer
 * @author Todd
 *
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
