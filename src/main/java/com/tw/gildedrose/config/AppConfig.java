package com.tw.gildedrose.config;

/**
 * A configuration class to configure the Spring context scanning
 * @author Todd
 */
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.tw.gildedrose")
public class AppConfig {
	
	

}