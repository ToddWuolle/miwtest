package com.tw.gildedrose.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tw.gildedrose.dao.ItemDAO;

/**
 * A Spring configuration bean for testing the ItemDAO class
 * @author Todd
 *
 */
@Configuration
public class ItemDAOTestConfig {

	@Bean
	public ItemDAO getItemDAO()
	{
		return new ItemDAO();
	}
}
