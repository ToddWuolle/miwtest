package com.tw.gildedrose.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.tw.gildedrose.model.Item;

/**
 * A Spring DAO Bean to provide access to the inventory of Item objects.
 * 
 * @author Todd
 *
 */
@Component
public class ItemDAO {

	private static Map<String, Item> items;

	{
		// initializing dummy database of items
		items = new HashMap<String, Item>();

		items.put("paper", new Item("paper", "ream of paper", 2));
		items.put("towel", new Item("towel", "a cloth towel", 5));
		items.put("bottle", new Item("bottle", "1.5L bottle", 2));

	}

	public List<Item> getItems() {
		List<Item> values = new ArrayList<Item>();

		values.addAll(items.values());

		return values;
	}

	public boolean updateInventory(String itemId) {
		boolean confirmValue = false;

		if (items != null) {
			Item foundItem = items.get(itemId);

			if (foundItem != null) {
				items.remove(itemId);
				confirmValue = true;
			}
		}
		return confirmValue;
	}
}
